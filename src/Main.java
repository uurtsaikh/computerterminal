import java.io.*;

public class Main {

    static int x = 0;   /* cursor x */
    static int y = 0;   /* cursor y */

    static final int SIZE = 10;
    static String[][] screen = new String[SIZE][SIZE];

    public static void main(String[] args) throws IOException {

        File file = new File(args[0]);

        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String line;
        StringBuilder builder = new StringBuilder();
        while ((line = buffer.readLine()) != null) {
            line = line.trim();
            builder.append(line);
        }

        for (String[] s : screen) {
            for (int j = 0; j < screen.length; j++) {
                s[j] = " ";
            }
        }

        process(builder);

    }

    static char element = 0;

    static void process(StringBuilder str) {
        if (str.length() > 0) {

            if (str.charAt(0) == '^') {
                element = str.charAt(1);

                switch (element) {
                    case 'c':
                        Screen.clear();
                        process(str.delete(0, 2));
                        break;
                    case 'h':
                        Cursor.cursor_reset();
                        process(str.delete(0, 2));
                        break;
                    case 'b':
                        Cursor.cursor_beginning();
                        process(str.delete(0, 2));
                        break;
                    case 'd':
                        Cursor.down();
                        process(str.delete(0, 2));
                        break;
                    case 'u':
                        Cursor.up();
                        process(str.delete(0, 2));
                        break;
                    case 'l':
                        Cursor.left();
                        process(str.delete(0, 2));
                        break;
                    case 'r':
                        Cursor.right();
                        process(str.delete(0, 2));
                        break;
                    case 'e':
                        Screen.erase();
                        process(str.delete(0, 2));
                        break;
                    case 'i':
                        Screen.MODE = "insert";
                        process(str.delete(0, 2));
                        break;
                    case 'o':
                        Screen.MODE = "overwrite";
                        process(str.delete(0, 2));
                        break;
                    case '^':
                        Screen.Circumflex();
                        process(str.delete(0, 2));
                        break;
                    default: {

                        int i = 1;

                        while (Character.isDigit(str.charAt(i))) {
                            if (i % 2 == 0) {
                                Cursor.cursor_set(Character.getNumericValue(str.charAt(i - 1)),
                                        Character.getNumericValue(str.charAt(i)));
                            }
                            i++;
                        }

                        process(str.delete(0, i));
                    }
                }
            } else {
                Screen.display(str.charAt(0));
                str = str.delete(0, 1);
                process(str);
            }
        } else {
            Screen.print();
        }
    }

    static class Cursor {

        static void up() {
            if (y > 0) {
                y--;
            }
        }

        static void down() {
            if (y < 9) {
                y++;
            }
        }

        static void right() {
            if (x < 9) {
                x++;
            }
        }

        static void left() {
            if (x > 0) {
                x--;
            }
        }

        static void cursor_beginning() {
            x = 0;
        }

        static void cursor_reset() {
            x = 0;
            y = 0;
        }

        static void cursor_set(int new_x, int new_y) {
            y = new_x;
            x = new_y;
        }
    }

    static class Screen {

        static String MODE = "overwrite"; // default value is overwrite

        static void erase() {
            for (int i = x; i <=9; i ++){
                screen[y][i] = " ";
            }
        }

        static void clear() {
            for (String[] s : screen) {
                for (int j = 0; j < screen.length; j++) {
                    s[j] = " ";
                }
            }
        }

        static void display(char c) {
            if(MODE.equals("overwrite")){
                screen[y][x] = String.valueOf(c);
            } else {
                for (int i = 9; i > x; i --){
                    screen[y][i] = screen[y][i-1];
                }
                screen[y][x] = String.valueOf(c);
            }
            
            Cursor.right();
        }

        static void Circumflex() {
            screen[y][x] = String.valueOf('^');
        }

        static void print() {
            for (int i = 0; i < SIZE; i++) {
                for (int j = 0; j < SIZE; j++) {
                    System.out.print(screen[i][j]);
                }
                System.out.println("");
            }
        }
    }
}
